// Copyright (c) 2024 Boomi, LP

package com.boomi.snowflake.util;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.snowflake.override.ConnectionOverrideUtil;
import com.boomi.snowflake.wrappers.SnowflakeWrapper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;

/**
 * Utility class for Snowflake operations.
 */
public final class SnowflakeOperationUtil {

    /** The Constant TWO. */
    private static final int TWO = 2;
    /** The Constant FOUR. */
    private static final int FOUR = 4;
    /** The Constant SIX. */
    private static final int SIX = 6;

    private SnowflakeOperationUtil() {
        // Prevent initialization
    }

    /**
     * Get the actual Snowflake table metadata for data type mapping
     *
     * @param objectTypeId table details
     * @param connection   Snowflake connection
     * @return metadata JSON object
     */
    public static SortedMap<String, String> getTableMetadata(String objectTypeId, Connection connection,
            String dataBase, String schema) {
        SortedMap<String, String> metadata = new TreeMap<>();
        String[] objectData = objectTypeId.split("\\.");
        try {
            if (null == dataBase && null == schema) {
                dataBase = connection.getCatalog();
                schema = connection.getSchema();
            } else {
                dataBase = ConnectionOverrideUtil.normalizeString(dataBase);
                schema = ConnectionOverrideUtil.normalizeString(schema);
            }
            try (ResultSet columnsRS = connection.getMetaData().getColumns(dataBase, schema,
                    objectData[0].replace("\"", ""), "%")) {
                if (columnsRS.next()) {
                    do {
                        metadata.putIfAbsent(columnsRS.getString(FOUR), columnsRS.getString(SIX));
                    } while (columnsRS.next());
                }
            }
        } catch (SQLException e) {
            throw new ConnectorException("Unable to connect: Unexpected database (and/or) schema name", e);
        }
        return metadata;
    }

    /**
     * Get snowflake table name from connection properties.
     *
     * @param properties the connection properties
     * @return table name
     */
    public static String getTableName(ConnectionProperties properties) {
        String tableName = properties.getTableName();
        if (tableName != null && tableName.lastIndexOf("\".\"") != -1) {
            tableName = tableName.substring(tableName.lastIndexOf("\".\"") + TWO);
        }
        return tableName;
    }

    /**
     * Sets up and returns snowflake wrapper.
     *
     * @param properties the connection properties
     * @return snowflake wrapper
     */
    public static SnowflakeWrapper setupWrapper(ConnectionProperties properties) {
        SnowflakeWrapper wrapper =
                new SnowflakeWrapper(properties.getConnectionGetter(), properties.getConnectionTimeFormat(),
                        properties.getLogger(), properties.getTableName());
        wrapper.setPreparedStatement(null);
        Long batchSize = properties.getBatchSize();
        if (batchSize > 1) {
            wrapper.setAutoCommit(false);
        }
        return wrapper;
    }

    /**
     * Handles ConnectorException for operations.
     *
     * @param response the operation response
     * @param request  the request data
     * @param e        the exception
     */
    public static void handleConnectorException(OperationResponse response, ObjectData request, ConnectorException e) {
        request.getLogger().log(Level.WARNING, e.getMessage());
        response.addResult(request, OperationStatus.APPLICATION_ERROR, e.getStatusCode(), e.getMessage(),
                ResponseUtil.toPayload(e.getMessage()));
    }

    /**
     * Handles general exception.
     *
     * @param response    the operation response
     * @param requestData the request data
     * @param e           the exception
     */
    public static void handleGeneralException(OperationResponse response, ObjectData requestData, Exception e) {
        requestData.getLogger().log(Level.SEVERE, e.getMessage());
        ResponseUtil.addExceptionFailure(response, requestData, e);
    }

    /**
     * Validates if either S3 Bucket or Stage name is set
     * @param properties the connection properties
     */
    public static void validateProperties(ConnectionProperties properties){
        if (properties.getBucketName().isEmpty() && properties.getStageName().isEmpty()) {
            throw new ConnectorException("Bucket Name and Stage Name empty. Enter a valid Bucket Name/Stage Name!");
        }
    }
}
