// Copyright (c) 2024 Boomi, LP
package com.boomi.snowflake.operations;

import java.io.InputStream;
import java.sql.Connection;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.logging.Level;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.SizeLimitedUpdateOperation;
import com.boomi.snowflake.SnowflakeConnection;
import com.boomi.snowflake.controllers.SnowflakeCreateController;
import com.boomi.snowflake.util.ConnectionProperties;
import com.boomi.snowflake.util.JSONHandler;
import com.boomi.snowflake.util.SnowflakeOperationUtil;
import com.boomi.snowflake.util.SnowflakeOverrideConstants;
import com.boomi.util.IOUtil;

/**
 * The Class SnowflakeCreateOperation.
 *
 * @author Vanangudi,S
 */
public class SnowflakeCreateOperation extends SizeLimitedUpdateOperation {

	/** The Constant APPLICATION_ERROR_MESSAGE. */
	private static final String APPLICATION_ERROR_MESSAGE = "Error in batch %d: ";
	/** The Constant TWO. */
	private static final int TWO = 2;

	/**
	 * Instantiates a new Snowflake Create Operation
	 * @param conn Snowflake connection parameters
	 */
	@SuppressWarnings("unchecked")
	public SnowflakeCreateOperation(SnowflakeConnection conn) {
		super(conn);
	}

	/**
	 * This function is called when CREATE operation gets called
	 * @param request
	 * 			Update Request
	 * @param Response
	 * 			Update Response
	 */
	@Override
	protected void executeSizeLimitedUpdate(UpdateRequest request, OperationResponse response) {
		ConnectionProperties properties = null;
		SnowflakeCreateController controller = null;
		try {
			PropertyMap operationProperties=getContext().getOperationProperties();
			properties = new ConnectionProperties(getConnection(), operationProperties,
					getContext().getObjectTypeId(), response.getLogger());
			controller = new SnowflakeCreateController(properties);
			Iterator<ObjectData> requestDataIterator = request.iterator();
			String tableName = properties.getTableName();
			if (tableName != null && tableName.lastIndexOf("\".\"") != -1) {
				tableName = tableName.substring(tableName.lastIndexOf("\".\"")+TWO);
			}
			SortedMap<String, String> metadata = null;
			if (tableName != null) {
				//to get meta data use override if present else use connection properties
				metadata = SnowflakeOperationUtil.getTableMetadata(tableName,
						properties.getConnectionGetter().getConnection(properties.getLogger()),
						operationProperties.getProperty(SnowflakeOverrideConstants.DATABASE),
						operationProperties.getProperty(SnowflakeOverrideConstants.SCHEMA));
			}
			executeUpdate(response, properties, controller, requestDataIterator, metadata);
		} catch (Exception e) {
			throw new ConnectorException(e);
		} finally {
			/*
			 * finalizes closes controller if it's not null
			 * otherwise commit and close properties
			 */
			if (controller == null) {
				if (properties != null) {
					properties.commitAndClose();
				}
			} else {
				controller.closeResources();
			}
		}
	}

	private void executeUpdate(OperationResponse response, ConnectionProperties properties,
			SnowflakeCreateController controller, Iterator<ObjectData> requestDataIterator,
			SortedMap<String, String> metadata) {
		boolean lastRequestData;
		while (requestDataIterator.hasNext()) {
			ObjectData requestData = requestDataIterator.next();
			lastRequestData = !requestDataIterator.hasNext();
			requestData.getLogger().info("Parsing document");
			InputStream inputData = requestData.getData();
			InputStream result = null;
			try {
				controller.receive(JSONHandler.readSortedMap(inputData), properties.getEmptyValueInput(), metadata,
						requestData.getDynamicOperationProperties());
				if (lastRequestData) {
					controller.executeLastBatch();
				}

				if (properties.getReturnResults()) {
					result = controller.getResultFromStatement(lastRequestData);
					if (result != null) {
						ResponseUtil.addSuccess(response, requestData, "0", ResponseUtil.toPayload(result));
					} else {
						ResponseUtil.addEmptySuccess(response, requestData, null);
					}
				} else {
					ResponseUtil.addSuccess(response, requestData, "0", ResponseUtil.toPayload(inputData));
				}
			} catch (ConnectorException e) {
				String errorMessage =
						String.format(APPLICATION_ERROR_MESSAGE, controller.getCurrentBatch()) + e.getMessage();
				requestData.getLogger().log(Level.WARNING, errorMessage, e);
				response.addResult(requestData, OperationStatus.APPLICATION_ERROR, e.getStatusCode(), errorMessage,
						ResponseUtil.toPayload(errorMessage));
			} catch (Exception e) {
				SnowflakeOperationUtil.handleGeneralException(response, requestData, e);
			} finally {
				IOUtil.closeQuietly(inputData);
				IOUtil.closeQuietly(result);
			}
		}
	}

	/**
	 * Gets the connection.
	 *
	 * @return the Snowflake connection
	 */
	@Override
	public SnowflakeConnection getConnection() {
		return (SnowflakeConnection) super.getConnection();
	}
}