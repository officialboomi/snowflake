// Copyright (c) 2024 Boomi, LP.
package com.boomi.snowflake.operations;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Logger;
import java.util.Iterator;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.testutil.SimpleTrackedData;
import com.boomi.snowflake.controllers.SnowflakeCreateController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.boomi.snowflake.SnowflakeConnection;
import com.boomi.snowflake.util.ConnectionProperties;
import com.boomi.snowflake.util.ModifiedSimpleOperationResponse;
import com.boomi.snowflake.util.ModifiedUpdateRequest;
import com.boomi.snowflake.util.SnowflakeContextIT;
import com.boomi.snowflake.util.SnowflakeOperationUtil;
import org.powermock.reflect.Whitebox;

/**
 * The Class SnowflakeCreateTest.
 *
 * @author Vanangudi,S
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(SnowflakeOperationUtil.class)
public class SnowflakeCreateTest extends BaseTestOperation {
	/** The Constant TABLE_NAME. */
	private static final String TABLE_NAME = "CREATE_OPERATION_TESTER";
	/** The Constant EMPTY_JSON. */
	private static final String EMPTY_JSON = "{}";
	/** The Constant INCORRECT_COL_NAME_JSON. */
	private static final String INCORRECT_COL_NAME_JSON = "{\"incorrect\":\"asd\"}";
	/** The Constant COL_NAME_JSON. */
	private static final String COL_NAME_JSON = "{\"ID\":\"asd\"}";
	/** The Constant INCORRECT_FORMATTED_JSON. */
	private static final String INCORRECT_FORMATTED_JSON = "{abc}";
	/** The Constant DEFAULT_BATCH_SIZE. */
	private static final long DEFAULT_BATCH_SIZE = 3;
	/** The Constant DOC_COUNT. */
	private static final int DOC_COUNT = 5;
	/** The Constant EMPTY_FIELD_SELECTION. */
	private static final String EMPTY_FIELD_SELECTION = "inputOptionsForMissingFields";
	/** The Constant NULL_SELECTION. */
	public static final String NULL_SELECTION = "SELECT_NULL";
	/** The Constant DEFAULT_SELECTION. */
	public static final String DEFAULT_SELECTION = "SELECT_DEFAULT";
	/** The List of Input Stream. */
	private List<InputStream> _inputs;
	private SnowflakeCreateOperation _op;
	private ModifiedSimpleOperationResponse _response;
	private ModifiedUpdateRequest _request;
	private ConnectionProperties properties;
	private SortedMap<String, String> metaDataValues = new TreeMap<>();
	private ConnectionProperties.ConnectionGetter connectionGetter;
	private Connection connection;
	private Logger logger;
	private SnowflakeCreateOperation snowflakeCreateOperation;
	private SnowflakeCreateController mockController;
	private OperationResponse mockOperationResponse;

	@Rule
	public ExpectedException thrown = ExpectedException.none();


	/**
	 *Sets all the Connection properties
	 */
	@Before
	public void setup() {
		testContext = new SnowflakeContextIT(OperationType.CREATE, null);
		testContext.setObjectTypeId(TABLE_NAME);
		testContext.setBatchSize(DEFAULT_BATCH_SIZE);
		_op = Mockito.mock(SnowflakeCreateOperation.class);
		_response = new ModifiedSimpleOperationResponse();
		properties = Mockito.mock(ConnectionProperties.class);
		metaDataValues.put("key1", "metaData");
		connectionGetter = Mockito.mock(ConnectionProperties.ConnectionGetter.class);
		SnowflakeConnection snowflakeConnection = Mockito.mock(SnowflakeConnection.class);
		connection = Mockito.mock(Connection.class);
		logger = Mockito.mock(Logger.class);
		mockController = Mockito.mock(SnowflakeCreateController.class);
		mockOperationResponse = Mockito.mock(OperationResponse.class);
		snowflakeCreateOperation = new SnowflakeCreateOperation(snowflakeConnection);
		Mockito.when(properties.getConnectionGetter()).thenReturn(connectionGetter);
		Mockito.when(connectionGetter.getConnection(logger)).thenReturn(connection);
		PowerMockito.mockStatic(SnowflakeOperationUtil.class);
	}
	
	/**
	 * Checks if the create operation returns success when inserting one column
	 */
	@Test
	public void shouldReturnSuccessWhenInsertingOneCol() {
		testContext.addOperationProperty(EMPTY_FIELD_SELECTION, NULL_SELECTION );
		setInputs(new ByteArrayInputStream(COL_NAME_JSON.getBytes()));
		runSnowflakeOperation(_op, _request, _response, DOC_COUNT, 1, OperationStatus.SUCCESS);
		results = _response.getResults();
		assertOperation(OperationStatus.SUCCESS, 1, DOC_COUNT);
		assertConnectionIsClosed(_op.getConnection());
	}
	
	/**
	 * Checks if the create operation returns success when inserting one column with return results enabled
	 */
	@Test
	public void shouldReturnSuccessWhenInsertingOneColWithReturnResults() {
		testContext.addOperationProperty(EMPTY_FIELD_SELECTION, NULL_SELECTION );
		setInputs(new ByteArrayInputStream(COL_NAME_JSON.getBytes()));
		testContext.getOperationProperties().putIfAbsent("returnResults", true);
		runSnowflakeOperation(_op, _request, _response, DOC_COUNT, 1, OperationStatus.SUCCESS);
		results = _response.getResults();
		assertBatchOperation(OperationStatus.SUCCESS, DOC_COUNT, 1);
		assertConnectionIsClosed(_op.getConnection());
	}
	
	/**
	 * Checks if the create operation returns success when inserting all nulls in the column.
	 */
	@Test
	public void shouldReturnSuccessWhenInsertingAllNulls() {
		testContext.addOperationProperty(EMPTY_FIELD_SELECTION, NULL_SELECTION );
		setInputs(new ByteArrayInputStream(EMPTY_JSON.getBytes()));
		runSnowflakeOperation(_op, _request, _response, DOC_COUNT, 1, OperationStatus.SUCCESS);
		results = _response.getResults();
		assertOperation(OperationStatus.SUCCESS, 1, DOC_COUNT);
		assertConnectionIsClosed(_op.getConnection());
	}
	
	/**
	 * Checks if the create operation returns error when inserting one invalid JSON format input.
	 */
	@Test
	public void shouldAddApplicationErrorWhenOneInvalidInputJsonFormatted() {
		setInputs(new ByteArrayInputStream(EMPTY_JSON.getBytes()));
		_inputs.set(3,new ByteArrayInputStream(INCORRECT_FORMATTED_JSON.getBytes()));
		runSnowflakeOperation(_op, _request, _response, DOC_COUNT, 0, OperationStatus.APPLICATION_ERROR);
		results = _response.getResults();
		Assert.assertEquals(OperationStatus.APPLICATION_ERROR, results.get(3).getStatus());
		assertConnectionIsClosed(_op.getConnection());
	}
	
	/**
	 * Checks if the create operation returns error when inserting invalid JSON format input values.
	 */
	@Test
	public void shouldAddApplicationErrorWhenOneInvalidInputJsonValues() {
		setInputs(new ByteArrayInputStream(EMPTY_JSON.getBytes()));
		_inputs.set(3,new ByteArrayInputStream(INCORRECT_COL_NAME_JSON.getBytes()));
		runSnowflakeOperation(_op, _request, _response, DOC_COUNT, 0, OperationStatus.APPLICATION_ERROR);
		results = _response.getResults();
		Assert.assertEquals(OperationStatus.APPLICATION_ERROR, results.get(3).getStatus());
		assertConnectionIsClosed(_op.getConnection());
	}
	
	/**
	 * Should return success when inserting one col with null selection.
	 */
	@Test
	public void shouldReturnSuccessWhenInsertingOneColWithNullSelection() {
		testContext.addOperationProperty(EMPTY_FIELD_SELECTION, NULL_SELECTION );
		setInputs(new ByteArrayInputStream(COL_NAME_JSON.getBytes()));
		runSnowflakeOperation(_op, _request, _response, DOC_COUNT, 1, OperationStatus.SUCCESS);
		results = _response.getResults();
		assertOperation(OperationStatus.SUCCESS, 1, DOC_COUNT);
		assertConnectionIsClosed(_op.getConnection());
	}
	/**
	 * Should return success when inserting one col with default selection.
	 */
	@Test
	public void shouldReturnSuccessWhenInsertingOneColWithDefaultSelection() {
		testContext.addOperationProperty(EMPTY_FIELD_SELECTION, DEFAULT_SELECTION );
		setInputs(new ByteArrayInputStream(COL_NAME_JSON.getBytes()));
		runSnowflakeOperation(_op, _request, _response, DOC_COUNT, 1, OperationStatus.SUCCESS);
		results = _response.getResults();
		assertOperation(OperationStatus.SUCCESS, 1, DOC_COUNT);
		assertConnectionIsClosed(_op.getConnection());
	}
	
	/**
	 * Should return success when inserting one col with return results with default selection.
	 */
	@Test
	public void shouldReturnSuccessWhenInsertingOneColWithReturnResultsWithDefaultSelection() {
		testContext.addOperationProperty(EMPTY_FIELD_SELECTION, DEFAULT_SELECTION );
		setInputs(new ByteArrayInputStream(COL_NAME_JSON.getBytes()));
		testContext.getOperationProperties().putIfAbsent("returnResults", true);
		runSnowflakeOperation(_op, _request, _response, DOC_COUNT, 1, OperationStatus.SUCCESS);
		results = _response.getResults();
		assertBatchOperation(OperationStatus.SUCCESS, DOC_COUNT, 1);
		assertConnectionIsClosed(_op.getConnection());
	}
	
	/**
	 * Should return error when inserting all nulls with default selection.
	 */
	@Test
	public void shouldReturnErrorWhenInsertingAllNullsWithDefaultSelection() {
		testContext.addOperationProperty(EMPTY_FIELD_SELECTION, DEFAULT_SELECTION );
		setInputs(new ByteArrayInputStream(EMPTY_JSON.getBytes()));
		runSnowflakeOperation(_op, _request, _response, DOC_COUNT, 1, OperationStatus.APPLICATION_ERROR);
		results = _response.getResults();
		assertOperation(OperationStatus.APPLICATION_ERROR, 1, DOC_COUNT);
		assertConnectionIsClosed(_op.getConnection());
	}
    /**
     * Tests the successful execution of the `executeUpdate` method when results are returned.
     * This test verifies that the method interacts correctly with the controller to fetch results and execute the last batch.
     *
     * @throws Exception if any exception occurs during the test execution
     */
    @Test
    public void testExecuteUpdate_SuccessfulExecution() throws Exception {
        List<InputStream>inputStreams = new ArrayList<>();
        inputStreams.add(new ByteArrayInputStream(EMPTY_JSON.getBytes()));
        SimpleTrackedData simpleTrackedData = new SimpleTrackedData(1, inputStreams.get(0));
        Iterator<SimpleTrackedData> requestDataIterator = Arrays.asList(new SimpleTrackedData[] {simpleTrackedData}).iterator();
        InputStream mockResult = new ByteArrayInputStream("result data".getBytes());
        Mockito.when(mockController.getResultFromStatement(true)).thenReturn(mockResult);
        Mockito.when(properties.getReturnResults()).thenReturn(true);
        PowerMockito.doNothing().when(mockController).receive(Mockito.any(), Mockito.eq("NULL"),
                Mockito.eq(metaDataValues), Mockito.any());
        //invoked private Method
        Whitebox.invokeMethod(snowflakeCreateOperation, "executeUpdate",
                mockOperationResponse, properties, mockController, requestDataIterator, metaDataValues);
        Mockito.verify(mockController).executeLastBatch();
        Mockito.verify(mockController).getResultFromStatement(true);

    }

    /**
     * Tests the successful execution of the `executeUpdate` method when no results are returned (null result).
     * This test ensures that the method handles a null result correctly when the `returnResults` property is set to true.
     *
     * @throws Exception if any exception occurs during the test execution
     */
    @Test
    public void testExecuteUpdate_SuccessfulExecution_WithReturnResults_NullResult() throws Exception {
        InputStream mockResult = null;
        List<InputStream> inputStreams = new ArrayList<>();
        inputStreams.add(new ByteArrayInputStream(EMPTY_JSON.getBytes()));
        SimpleTrackedData inputDoc1 = new SimpleTrackedData(1,inputStreams.get(0) );
        Iterator<SimpleTrackedData> requestDataIterator = Arrays.asList(new SimpleTrackedData[]{inputDoc1}).iterator();
        Mockito.when(properties.getReturnResults()).thenReturn(true);
        Mockito.when(mockController.getResultFromStatement(true)).thenReturn(mockResult);
        PowerMockito.doNothing().when(mockController).receive(Mockito.any(), Mockito.eq("NULL"),
                Mockito.eq(metaDataValues), Mockito.any());
        Whitebox.invokeMethod(snowflakeCreateOperation, "executeUpdate", mockOperationResponse, properties,
                mockController, requestDataIterator, metaDataValues);

        Mockito.verify(mockController).executeLastBatch();
        Mockito.verify(mockController).getResultFromStatement(true);
    }

    /**
     * Tests the successful execution of the `executeUpdate` method when no results are returned.
     * This test ensures that the method behaves correctly when the `returnResults` property is set to false.
     *
     * @throws Exception if any exception occurs during the test execution
     */
    @Test
    public void testExecuteUpdate_SuccessfulExecution_NoReturnResults() throws Exception {
        List<InputStream> inputStreams = new ArrayList<>();
        inputStreams.add(new ByteArrayInputStream(EMPTY_JSON.getBytes()));
        SimpleTrackedData inputDoc1 = new SimpleTrackedData(1,inputStreams.get(0) );
        Iterator<SimpleTrackedData> requestDataIterator = Arrays.asList(new SimpleTrackedData[]{inputDoc1}).iterator();
        Mockito.when(properties.getReturnResults()).thenReturn(false);
        PowerMockito.doNothing().when(mockController).receive(Mockito.any(), Mockito.eq("NULL"),
                Mockito.eq(metaDataValues),Mockito.any());
        Whitebox.invokeMethod(snowflakeCreateOperation, "executeUpdate", mockOperationResponse, properties,
                mockController, requestDataIterator, metaDataValues);

        Mockito.verify(mockController).executeLastBatch();
    }

    /**
     * Tests the `executeUpdate` method when a `ConnectorException` is thrown during execution.
     * This test verifies that the exception is handled properly and that the method behaves as expected when the controller's receive method throws an exception.
     *
     * @throws Exception if any exception occurs during the test execution
     */
    @Test(expected = Exception.class)
    public void testExecuteUpdate_ConnectorException() throws Exception {
        Mockito.when(properties.getReturnResults()).thenReturn(true);
        Mockito.when(mockController.getResultFromStatement(false)).thenReturn(null);
        Mockito.doThrow(new ConnectorException("Connector exception occurred"))
                .when(mockController)
                .receive(Mockito.any(), Mockito.any(), Mockito.any(),Mockito.any());

        thrown.expectMessage("Connector exception occurred");

        mockController.receive((Mockito.any()), Mockito.any(), Mockito.any(),Mockito.any());
    }

	/**
	 *  Test before calling getTablemetadata -connection is updated with new operation properties
	 */
	@Test
	public void testConnectionForCreateOperationMetadata() throws Exception {
		testContext.addOperationProperty(EMPTY_FIELD_SELECTION, DEFAULT_SELECTION );
		setInputs(new ByteArrayInputStream(EMPTY_JSON.getBytes()));
		SnowflakeConnection mockSnowflakeConnection = PowerMockito.mock(SnowflakeConnection.class);
		SnowflakeCreateOperation snowflakeCreateOperation = new SnowflakeCreateOperation(mockSnowflakeConnection);
		testContext = new SnowflakeContextIT(OperationType.CREATE, null);
		testContext.setObjectTypeId(TABLE_NAME);
		UpdateRequest mockRequest = PowerMockito.mock(UpdateRequest.class);
		OperationResponse mockResponse = PowerMockito.mock(OperationResponse.class);
		Mockito.when(mockResponse.getLogger()).thenReturn(Logger.getLogger(SnowflakeCreateOperation.class.getName()));;
		Mockito.when(mockSnowflakeConnection.getContext()).thenReturn(testContext);
		Mockito.when(mockSnowflakeConnection.getOperationContext()).thenReturn(testContext);

		ConnectionProperties connectionPropertiesMock = PowerMockito.mock(ConnectionProperties.class);
		Logger mockLogger = PowerMockito.mock(Logger.class);
		PowerMockito.whenNew(ConnectionProperties.class)
				.withArguments(mockSnowflakeConnection, testContext.getOperationProperties(), testContext.getObjectTypeId(),
						mockLogger)
				.thenReturn(connectionPropertiesMock);
		Connection connectionMock = PowerMockito.mock(Connection.class);
		Mockito.when(mockSnowflakeConnection.createJdbcConnection()).thenReturn(connectionMock);
		testContext.addOperationProperty("db", "db1" );
		testContext.addOperationProperty("schema", "schema1" );
		PowerMockito.mockStatic(SnowflakeOperationUtil.class);
		SortedMap<String, String> metadata = null;
		PowerMockito.when(
						SnowflakeOperationUtil.getTableMetadata("CREATE_OPERATION_TESTER", connectionMock,
								"DB", "SCHEMA"))
				.thenReturn(metadata);
		Iterator<ObjectData> mockIterator = Mockito.mock(Iterator.class);
		Mockito.when(mockRequest.iterator()).thenReturn(mockIterator);
		Mockito.when(mockIterator.hasNext()).thenReturn(false);
		//call actual method
		snowflakeCreateOperation.executeSizeLimitedUpdate(mockRequest,mockResponse);
		//verify
		Mockito.verify(connectionMock,Mockito.times(1)).setCatalog("test_DB");
		Mockito.verify(connectionMock,Mockito.times(1)).setSchema("PUBLIC");
	}

	/**
	 * Sets the Create operation input.
	 * @param value
	 * 			Input Stream for the create operation
	 */
	private void setInputs(InputStream value) {
		_inputs = new ArrayList<>();
		for(int i = 0 ; i < DOC_COUNT; i++) {
			_inputs.add(value);
		}
		_request = new ModifiedUpdateRequest(_inputs, _response);
	}
}
